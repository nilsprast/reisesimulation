'''
Nebenklasse der Klasse Fahrzeug 
@author: nils
'''
from fahrzeug import Fahrzeug
from random import randint

class Auto(Fahrzeug):

    def __init__(self):
        super().__init__()
        self.AutoPlatz = 50
        self.AutoPreis = randint(1000,5000)
      
    def getAutoPlatz(self):
        return self.AutoPlatz
      
    def getAutoPreis(self):
        return self.AutoPreis
