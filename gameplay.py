'''
Die Game-Klasse in der der komplette Spielablauf ist!
@author: Max Morandell, Hakan Yazici
'''

#Klassen und Funktionen werden importiert.
from kontinente import Kontinente
from afrika import Afrika
from amerika import Amerika
from asien import Asien
from europa import Europa
from charaktere import Character
from person1 import Person1
from person2 import Person2
from person3 import Person3
from moped import Moped
from auto import Auto
from flugzeug import Flugzeug
from random import randint
import sys

#Erstellung der Hauptvariablen
Fahrzeuge = {'moped' : 0, 'auto' : 0, 'flugzeug' : 0}
Waren = {'Gewürzmischung' : 0, 'Waffen' : 0, 'Uhren' : 0, 'Vodka' : 0}
derzeitigerKontinent = None

#Auswahl des Charakters
def charakterWaehlen():
    
    global derzeitigerCharakter
    global Geld
    
    while(True):
        print("Wählen Sie bitte einen Charakter: \n")
        print("Zur Auswahl stehen 3 verschiedene Charaktere:")
        print("Für den Ersten druecken Sie bitte die [1]")
        print("Für den Zweiten druecken Sie bitte die [2]")
        print("Für den Dritten druecken Sie bitte die [3]")
        print("________________________________________________")
        ihreWahl = input()
        
        if(ihreWahl == "1"):
            derzeitigerCharakter = Person1()
            print("Sie haben den Ersten gewaehlt:")
            print("Dieser besitzt wenig Geld und ein erhöhtes Risiko!")
            print("________________________________________________")
            Geldstart = Person1()
            Geld = Geldstart.getmoney()
            break
        if(ihreWahl == "2"):
            derzeitigerCharakter = Person2()
            print("Sie haben den Zweiten gewaehlt:")
            print("Dieser besitzt etwas Geld und ein durchschnittliches Risiko!")
            print("________________________________________________")
            Geldstart = Person2()
            Geld = Geldstart.getmoney()
            break
        if(ihreWahl == "3"):
            derzeitigerCharakter = Person3()
            print("Sie haben den Dritten gewaehlt:")
            print("Dieser besitzt viel Geld und ein minimales Risiko!")
            print("________________________________________________")
            Geldstart = Person3()
            Geld = Geldstart.getmoney()
            break
        print("Bitte geben Sie eine Zahl zwischen 1 & 3 ein!")
        print("________________________________________________")


def kontinentErstellen():
    
    global derzeitigerKontinent
    
    while(True):
        print("Wählen Sie bitte einen Kontinent")
        print("Zur Auswahl stehen die Kontinente Afrika, Asien, Amerika, Europa ")
        print("Für Afrika druecken Sie bitte die [1]")
        print("Für Amerika druecken Sie bitte die [2]")
        print("Für Asien druecken Sie bitte die [3]")
        print("Für Europa druecken Sie bitte die [4]")
        print("________________________________________________")
        ihreWahl = input()
        
        if(ihreWahl == "1"):
            derzeitigerKontinent = Afrika()
            print("Sie haben das Thema Afrika gewählt")
            print("________________________________________________")
            print("________________________________________________")
            break
        if(ihreWahl == "2"):
            derzeitigerKontinent = Amerika()
            print("Sie haben das Thema Amerika gewählt")
            print("________________________________________________")
            print("________________________________________________") 
            break
        if(ihreWahl == "3"):
            derzeitigerKontinent = Asien()
            print("Sie haben das Thema Asien gewählt")
            print("________________________________________________")
            print("________________________________________________")
            break
            
        if(ihreWahl == "4"):
            derzeitigerKontinent = Europa()
            print("Sie haben das Thema Europa gewählt")
            print("________________________________________________")
            print("________________________________________________")
            break
        print("Bitte geben Sie eine Zahl zwischen 1 & 4 ein!")
        print("________________________________________________")
        print("________________________________________________")
        
 
#Funktion um Fahrzeug zu kaufen      
def fahrzeugKaufen():
    
    global Geld
    global Fahrzeuge
    MPreis = Moped()
    MopedPreis = MPreis.getMopedPreis()
    APreis = Auto()
    AutoPreis = APreis.getAutoPreis()
    FPreis = Flugzeug()
    FlugzeugPreis = FPreis.getFlugzeugPreis()
        
    print("Wählen Sie bitte ein Fahrzeug das Sie kaufen moechten:")
    print("Zur Auswahl stehen ein Moped, ein Auto und ein Flugzeug \n")
    print("Diese Fahrzeuge besitzen Sie [0]")
    print("Für das Moped druecken Sie bitte die [1]")
    print("Für das Auto druecken Sie bitte die [2]")
    print("Für das Flugzeug druecken Sie bitte die [3]")
    print("Preis Moped:" + str(MopedPreis) + ", Preis Auto:"+ str(AutoPreis) + ", Preis Flugzeug:" + str(FlugzeugPreis))
    print("Aktuelles Guthaben:" + str(Geld))
    print("________________________________________________")
    
    ihreWahl = input()
    
    if(ihreWahl == "0"):
        print("Diese Fahrzeuge besitzen Sie! \n") 
        print(Fahrzeuge)
        print("________________________________________________")
            
        
    elif(ihreWahl == "1"):
        if(Geld >= MopedPreis):
            Geld = Geld - MopedPreis
            Fahrzeuge['moped'] += 1
            print("Sie haben ein Moped gekauft \n")
            print("________________________________________________")
            
        else:
            print("Ihr Guthaben ist zu klein \n")
            print("________________________________________________")
            
                
    elif(ihreWahl == "2"):
        if(Geld >= AutoPreis):
            Geld = Geld - AutoPreis
            Fahrzeuge['auto'] += 1 
            print("Sie haben ein Auto gekauft \n") 
            print("________________________________________________")
            
        else: 
            print("Ihr Guthaben ist zu klein \n")
            print("________________________________________________")
            
    elif(ihreWahl == "3"):
        if(Geld >= FlugzeugPreis):
            Geld = Geld - FlugzeugPreis
            Fahrzeuge['flugzeug'] += 1 
            print("Sie haben ein Flugzeug gekauft \n") 
            print("________________________________________________")
            
        else: 
            print("Ihr Guthaben ist zu klein \n")
            print("________________________________________________")
               
    else:         
        print("Bitte geben Sie eine Zahl zwischen 0 & 3 ein! \n")
        print("________________________________________________")
        
#Funktion um Waren zu kaufen 
def warenKaufen():
    
    global derzeitigerKontinent
    global Waren
    global Geld
    
    Preise = derzeitigerKontinent.getPreis()
    
    MLager = Moped()
    LagerMoped = MLager.getMopedPlatz()
    ALager = Auto()
    LagerAuto = ALager.getAutoPlatz()
    FLager = Flugzeug()
    LagerFlugzeug = FLager.getFahrzeugPlatz()
    Platz = LagerMoped + LagerAuto + LagerFlugzeug
    
    print("Wählen Sie bitte eine Ware um diese zu erwerben:")
    print("Zur Auswahl stehen Gewürzmischung, Waffen, Uhren & Vodka  \n")
    print("Diese Waren besitzen Sie [0]")
    print("Um Gewürzmischung zu kaufen druecken Sie bitte die [1]")
    print("Um Waffen zu kaufen druecken Sie bitte die [2]")
    print("Um Uhren zu kaufen druecken Sie bitte die [3]")
    print("Um Vodka zu kaufen druecken Sie bitte die [4]")
    print("Aktuelle Waren Preise :" + str(Preise))
    print("Aktuelles Guthaben:" + str(Geld))
    print("________________________________________________")
    
    ihreWahl = input()
    
    if(ihreWahl == "0"):
        print("Diese Waren besitzen Sie! \n") 
        print(Waren)
        print("________________________________________________")
            
            
    elif(ihreWahl == "1"):
        print("Nun bitte Anzahl der Ware angeben: \n")
        anzahl = input()
        if(Geld >= Preise['Gewürzmischung'] * int(anzahl) and Platz >= int(anzahl)):
            Geld = Geld - Preise['Gewürzmischung'] * int(anzahl)
            Waren['Gewürzmischung'] += 1 * int(anzahl)
            print("Sie haben " + str(anzahl) + " Gewürzmischung gekauft")
            print("________________________________________________")
            
        else:
            print("Ihr Guthaben/Ladefläche ist zu klein")
            print("________________________________________________")
                
    elif(ihreWahl == "2"):
        print("Nun bitte Anzahl der Ware angeben: \n")
        anzahl = input()
        if(Geld >= Preise['Waffen'] * int(anzahl) and Platz >= int(anzahl)):
            Geld = Geld - Preise['Waffen'] * int(anzahl)
            Waren['Waffen'] += 1 * int(anzahl)
            print("Sie haben " + str(anzahl) + " Waffen gekauft") 
            print("________________________________________________")
            
        else:
            print("Ihr Guthaben/Ladefläche ist zu klein")
            print("________________________________________________")

        
    elif(ihreWahl == "3"):
        print("Nun bitte Anzahl der Ware angeben: \n")
        anzahl = input()
        if(Geld >= Preise['Uhren'] * int(anzahl) and Platz >= int(anzahl)):
            Geld = Geld - Preise['Uhren']* int(anzahl)
            Waren['Uhren'] += 1 * int(anzahl)
            print("Sie haben " + str(anzahl) + " Uhren gekauft")
            print("________________________________________________")
            
        else:
            print("Ihr Guthaben/Ladefläche ist zu klein")
            print("________________________________________________")
            
            
    elif(ihreWahl == "4"):
        print("Nun bitte Anzahl der Ware angeben: \n")
        anzahl = input()
        if(Geld >= Preise['Vodka'] * int(anzahl) and Platz >= int(anzahl)):
            Geld = Geld - Preise['Vodka']* int(anzahl)
            Waren['Vodka'] += 1 * int(anzahl)
            print("Sie haben " + str(anzahl) + " Vodka gekauft")
            print("________________________________________________")
            
        else:
            print("Ihr Guthaben/Ladefläche ist zu klein")
            print("________________________________________________")
            
    else:         
        print("Bitte geben Sie eine Zahl zwischen 0 & 4 ein!")
        print("________________________________________________")
    


#Funktion um Waren zu verkaufen
def warenVerkaufen():
    
    global derzeitigerKontinent
    global Waren
    global Geld
    Preise = derzeitigerKontinent.getPreis()
    
    print("Wählen Sie bitte eine Ware um diese zu verkaufen: \n")
    print("Zur Auswahl stehen Gewürzmischung, Waffen, Uhren & Vodka \n")
    print("Diese Waren besitzen Sie [0]")
    print("Um Gewürzmischung zu verkaufen druecken Sie bitte die [1]")
    print("Um Waffen zu verkaufen druecken Sie bitte die [2]")
    print("Um Uhren zu verkaufen druecken Sie bitte die [3]")
    print("Um Vodka zu verkaufen druecken Sie bitte die [4]")
    print("Aktuelle Waren Preise :" + str(Preise))
    print("Aktuelles Guthaben:" + str(Geld))
    print("________________________________________________")
    ihreWahl = input()
       
    if(ihreWahl == "0"):
        print("Diese Waren besitzen Sie!") 
        print(Waren)
        print("________________________________________________")
            
            
    elif(ihreWahl == "1"):
        print("Nun bitte Anzahl der Ware angeben:")
        anzahl = input()
        if(Waren['Gewürzmischung'] >= int(anzahl)):
            Geld = Geld + Preise['Gewürzmischung'] * int(anzahl)
            Waren['Gewürzmischung'] -= 1 * int(anzahl)
            print("Sie haben " + str(anzahl) + " Gewürzmischung verkauft")
            print("________________________________________________")
            
        else: 
            print("Sie können nicht mehr Gewürzmischung verkaufen als Sie besitzen")
            print("________________________________________________")
            
                
    elif(ihreWahl == "2"):
        print("Nun bitte Anzahl der Ware angeben:")
        anzahl = input()
        if(Waren['Waffen'] >= int(anzahl)):
            Geld = Geld + Preise['Waffen'] * int(anzahl)
            Waren['Waffen'] -= 1 * int(anzahl)
            print("Sie haben " + str(anzahl) + " Waffen verkauft") 
            print("________________________________________________")
            
        else:
            print("Sie können nicht mehr Waffen verkaufen als Sie besitzen")
            print("________________________________________________")
        
        
    elif(ihreWahl == "3"):
        print("Nun bitte Anzahl der Ware angeben:")
        anzahl = input()
        if(Waren['Uhren'] >= int(anzahl)):
            Geld = Geld + Preise['Uhren']* int(anzahl)
            Waren['Uhren'] -= 1 * int(anzahl)
            print("Sie haben"+ str(anzahl) + " Uhren verkauft")
            print("________________________________________________")
            
        else:
            print("Sie können nicht mehr Uhren verkaufen als Sie besitzen")
            print("________________________________________________")
            
            
    else:         
        print("Bitte geben Sie eine Zahl zwischen 0 & 4 ein!")
        print("________________________________________________")
  
#Funktion um den Lagerstand zu erfahren   
def warenStand():
    
    global Waren
    
    print("Diese Waren besitzen Sie!") 
    print(Waren)
    print("________________________________________________")

#Funktion um den Kontostand zu erfahren
def kontostand():
    
    global Geld
    
    print("Das ist Ihr aktueller Kontostand:")
    print(Geld)
    print("________________________________________________")

#Funktion um zu erfahren welche Fahrzeuge man besitzt
def fahrzeugStand():
    
    global Fahrzeuge
    
    print("Diese Fahrzeuge besitzen Sie!") 
    print(Fahrzeuge)
    print("________________________________________________")

  
#Funktion um weiter zu Reisen      
def reisen():
    
    global Geld
    
   # zollKontrolle()
    Geld = Geld - 100
    kontinentErstellen()


#Spielmenue
def menu():
    
    
    ihreWahl = 0
    

    while(True): 
        print("Was moechten Sie nun tun?")
        print("Zur Auswahlt steht: \n")
        print("Ein Fahrzeug zu kaufen [1]")
        print("Waren kaufen [2]")
        print("Waren verkaufen [3]")
        print("Ihr Lager zu überprüfen [4]")
        print("Ihren Kontostand zu überprüfen [5]")
        print("Ihre Fahrzeuge ansehen [6]")
        print("Weiter reisen [7]")
        
        ihreWahl = input()
        if(ihreWahl == "1"):
            fahrzeugKaufen()
        if(ihreWahl == "2"):
            warenKaufen()
        if(ihreWahl == "3"):
            warenVerkaufen()
        if(ihreWahl == "4"):
            warenStand()
        if(ihreWahl == "5"):
            kontostand()
        if(ihreWahl == "6"):
            fahrzeugStand()
        if(ihreWahl == "7"):
            reisen()
            
         
#main        
print("#### Willkommen zu unserem Weltraumsimulator ####")
charakterWaehlen()
kontinentErstellen()
menu()