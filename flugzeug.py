'''
Nebenklasse der Klasse Fahrzeug.
@author: nils
'''
from fahrzeug import Fahrzeug
from random import randint

class Flugzeug(Fahrzeug):

    def __init__(self):
        super().__init__()
        self.FlugzeugPlatz = 200
        self.FlugzeugPreis = randint(7000,12000)

    def getFlugzeugPlatz(self):
        return self.FlugzeugPlatz 

    def getFlugzeugPreis(self):
        return self.FlugzeugPreis