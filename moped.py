'''
Nebenklasse der Klasse Fahrzeug !
@author: nils
'''
from fahrzeug import Fahrzeug
from random import randint

class Moped(Fahrzeug):

    def __init__(self):
        super().__init__()
        self.MopedPlatz = 5
        self.MopedPreis = randint(200,500)

    def getMopedPlatz(self):
        return self.MopedPlatz 

    def getMopedPreis(self):
        return self.MopedPreis