'''
Created on 27.11.2018
Nebenklasse für Kontinent Asien
@author: Stefan
'''

from kontinente import Kontinente
from random import randint

class Asien(Kontinente):
     
    def __init__(self):
        super().__init__()
        self.setPreis()
    
    def setPreis(self):
        self.kontinentPreis['Gewürzmischung'] =  randint(10,25)
        self.kontinentPreis['Waffen'] = randint(500,750)
        self.kontinentPreis['Uhren'] = randint(7,35)
        self.kontinentPreis['Vodka'] = randint(70,100)

    def getPreis(self):
        return Kontinente.getPreis(self)