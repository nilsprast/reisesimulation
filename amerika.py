'''
Created on 27.11.2018
Nebenklasse für Kontinent Amerika
@author: Stefan
'''
from kontinente import Kontinente
from random import randint

class Amerika(Kontinente):
     
    def __init__(self):
        super().__init__()
        self.setPreis()
    
    def setPreis(self):
        self.kontinentPreis['Gewürzmischung'] =  randint(50,75)
        self.kontinentPreis['Waffen'] = randint(100,250)
        self.kontinentPreis['Uhren'] = randint(20,55)
        self.kontinentPreis['Vodka'] = randint(700,1050)

    def getPreis(self):
        return Kontinente.getPreis(self)