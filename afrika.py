'''
Created on 27.11.2018
Nebenklasse für Kontinent Afrika
@author: Stefan
'''

from kontinente import Kontinente
from random import randint

class Afrika(Kontinente):
     
    def __init__(self):
        super().__init__()
        self.setPreis()
    
    def setPreis(self):
        self.kontinentPreis['Gewürzmischung'] =  randint(5,15)
        self.kontinentPreis['Waffen'] = randint(60,100)
        self.kontinentPreis['Uhren'] = randint(750,950)
        self.kontinentPreis['Vodka'] = randint(25,50)

    def getPreis(self):
        return Kontinente.getPreis(self)