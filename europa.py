'''
Created on 27.11.2018
Nebenklasse für Kontinent Europa
@author: Stefan
'''
from kontinente import Kontinente
from random import randint

class Europa(Kontinente):
     
    def __init__(self):
        super().__init__()
        self.setPreis()
    
    def setPreis(self):
        self.kontinentPreis['Gewürzmischung'] =  randint(900,1250)
        self.kontinentPreis['Waffen'] = randint(500,750)
        self.kontinentPreis['Uhren'] = randint(15,35)
        self.kontinentPreis['Vodka'] = randint(5,25)

    def getPreis(self):
        return Kontinente.getPreis(self)