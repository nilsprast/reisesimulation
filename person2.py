'''
Created on 02.12.2018

@author: Saidi
'''
from charaktere import Character
from random import randint

class Person2(Character):
    
    def __init__(self):
        super().__init__()
        self.setmoney()
        self.setrisk()
        
    def setmoney(self):
        self.money = 5000
    
    def setrisk(self):
        self.risk['Risiko'] = randint(5,15)
                 
    def getmoney(self):
        return Character.getMoney(self)
    
    def getrisk(self):
        return Character.getRisk(self)