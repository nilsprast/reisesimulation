'''
Created on 02.12.2018

@author: Saidi
'''
from charaktere import Character
from random import randint

class Person3(Character):
    def __init__(self):
        super().__init__()
        self.setmoney()
        self.setrisk()
        
    def setmoney(self):
        self.money = 10000
    
    def setrisk(self):
        self.risk['Risiko'] = randint(8,12)
                 
    def getmoney(self):
        return Character.getMoney(self)
    
    def getrisk(self):
        return Character.getRisk(self)