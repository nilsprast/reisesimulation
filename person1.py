'''
Created on 27.11.2018

@author: Saidi
''' 
from charaktere import Character
from random import randint

class Person1(Character):
    
    def __init__(self):
        super().__init__()
        self.setmoney()
        self.setrisk()
        
    def setmoney(self):
        self.money = 1500
    
    def setrisk(self):
        self.risk['Risiko'] = randint(0,20)
                 
    def getmoney(self):
        return Character.getMoney(self)
    
    def getrisk(self):
        return Character.getRisk(self)